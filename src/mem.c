#include <stdarg.h>

#define _DEFAULT_SOURCE

#include <unistd.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <assert.h>
#include <sys/mman.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header *b, const char *fmt, ...);//ваще не понял
void debug(const char *fmt, ...);//см выше

extern inline block_size size_from_capacity(block_capacity cap);

//https://coderoad.ru/62399410/%D0%9A%D0%B0%D0%BA-%D0%BE%D0%B1%D1%8A%D1%8F%D0%B2%D0%BB%D0%B5%D0%BD%D0%B8%D0%B5-extern-inline-%D0%B2%D1%8B%D0%B7%D1%8B%D0%B2%D0%B0%D0%B5%D1%82-%D0%B3%D0%B5%D0%BD%D0%B5%D1%80%D0%B0%D1%86%D0%B8%D1%8E-%D0%BA%D0%BE%D0%B4%D0%B0
extern inline block_capacity capacity_from_size(block_size sz);

static bool block_is_big_enough(size_t query, struct block_header *block) { return block->capacity.bytes >= query; }

static size_t pages_count(size_t mem) { return mem / getpagesize() + ((mem % getpagesize()) > 0); }

//The getpagesize() function returns the number of bytes in a page.  Page
//granularity is the granularity of many of the memory management calls.
//
//The page size is a system page size and may not be the same as the
//underlying hardware page size.
static size_t round_pages(size_t mem) { return getpagesize() * pages_count(mem); }

//генерация нового блока по адресу предыдущего
static void block_init(void *restrict addr, block_size block_sz, void *restrict next) {

    *((struct block_header *) addr) = (struct block_header) {
            .next = next,
            .capacity = capacity_from_size(block_sz),
            .is_free = true
    };printf("checkpoint1\n");
}

static size_t region_actual_size(size_t query) { return size_max(round_pages(query), REGION_MIN_SIZE); }

extern inline bool region_is_invalid(const struct region *r);


static void *map_pages(void const *addr, size_t length, int additional_flags) {
    return mmap((void *) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, 0, 0);
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region(void const *addr, size_t query) {

    size_t ac_size = region_actual_size(query);

    void *addr_map = map_pages(addr, ac_size, MAP_FIXED);//may be MAP_FIXED_NOREPLACE

    //probably should add check if ()

    block_init(addr_map, (block_size) {ac_size}, NULL);
    printf("checkpoint2\n");
    struct region region = {addr_map, ac_size, false};

    return region;
}

static void *block_after(struct block_header const *block);

void *heap_init(size_t initial) {
    const struct region region = alloc_region(HEAP_START, initial);
    if (region_is_invalid(&region)) return NULL;

    return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable(struct block_header *restrict block, size_t query) {
    return block->is_free &&
           query + offsetof(struct block_header, contents) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

//видимо надо прописать, что если слишком много то мы делим его на элементы списка
static bool split_if_too_big(struct block_header *block, size_t query) {
    if (block_splittable(block, query)) {

        void *new_block = (void *) ((uint8_t *) block + query + offsetof(struct block_header, contents));
        block_init(new_block, (block_size) {region_actual_size(block->capacity.bytes - query)}, NULL);
        block->next = new_block;
        block->capacity.bytes = query;
        return true;
    }
    return false;
}


/*  --- Слияние соседних свободных блоков --- */

static void *block_after(struct block_header const *block) {
    return (void *) (block->contents + block->capacity.bytes);
}

static bool blocks_continuous(
        struct block_header const *fst,
        struct block_header const *snd) {
    return (void *) snd == block_after(fst);
}

static bool mergeable(struct block_header const *restrict fst, struct block_header const *restrict snd) {
    return fst->is_free && snd->is_free && blocks_continuous(fst, snd);
}

static bool try_merge_with_next(struct block_header *block) {
    if (block->next) {
        if (mergeable(block, block->next)) {
            //block = block_after(block);
            block->capacity.bytes =
                    block->capacity.bytes + block->next->capacity.bytes + offsetof(struct block_header, contents);
            block->next = block->next->next;
            return true;
        }
    }
    return false;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
    enum {
        BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED
    } type;
    struct block_header *block;
};


static struct block_search_result find_good_or_last(struct block_header *restrict block, size_t sz) {
    //struct block_header* first_block = block;
    struct block_search_result res = {.type= BSR_REACHED_END_NOT_FOUND, .block = block};//It definitly won't work, but I'm not sure
    while ((block->next && block_is_big_enough(sz, block))) {
        if (try_merge_with_next(block)) {
            res.type = BSR_FOUND_GOOD_BLOCK;
        }
        block = block->next;
    }
    if (block_is_big_enough(sz, res.block)) res.type = BSR_FOUND_GOOD_BLOCK;
    return res;
}


/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing(size_t query, struct block_header *block) {
    struct block_search_result res = find_good_or_last(block, query);
    if (res.type == BSR_FOUND_GOOD_BLOCK) split_if_too_big(res.block, query);
    return res;

}

struct region heap_continue(struct block_header *restrict last, size_t query) {
    struct region r = {.addr = (void *) ((uint8_t *) last + size_from_capacity(last->capacity).bytes),
            .size = size_from_capacity((block_capacity) {query}).bytes,
            .extends=true};//also definitely not working shit
    //block_init(r.addr, (block_size){r.size}, NULL);
    //struct block_header* new_block = (struct block_header*) r.addr;

    return r;

}

static struct block_header *grow_heap(struct block_header *restrict last, size_t query) {
    struct region r = heap_continue(last, query);
    block_init(r.addr, (block_size) {r.size}, NULL);
    struct block_header *new = (struct block_header *) r.addr;

    last->next = new;
    if (try_merge_with_next(last)) {
        return last;
    } else {
        return new;
    }

}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header *memalloc(size_t query, struct block_header *heap_start) {
    struct block_search_result block_h = try_memalloc_existing(query, heap_start);
    if (block_h.type == BSR_REACHED_END_NOT_FOUND) {
        block_h.block = grow_heap(block_h.block, size_max(query, BLOCK_MIN_CAPACITY));
        split_if_too_big(block_h.block, size_max(query, BLOCK_MIN_CAPACITY));
    }
    if (block_h.type == BSR_CORRUPTED) {
        block_h.block = heap_init(size_max(query, BLOCK_MIN_CAPACITY));
        split_if_too_big(block_h.block, size_max(query, BLOCK_MIN_CAPACITY));
    }
    block_h.block->is_free = false;
    return block_h.block;
}

void *_malloc(size_t query) {
    struct block_header *const addr = memalloc(query, (struct block_header *) HEAP_START);
    if (addr) return addr->contents;
    else return NULL;
}

struct block_header *block_get_header(void *contents) {
    return (struct block_header *) (((uint8_t *) contents) - offsetof(struct block_header, contents));
}

void _free(void *mem) {
    if (!mem) return;
    struct block_header *header = block_get_header(mem);
    header->is_free = true;
    /*  ??? */
    while (try_merge_with_next(header));
}
