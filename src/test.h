#ifndef TEST_H
#define TEST_H

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

enum test_result {
    TEST_PASSED = 0,
    TEST_FAILED
};

enum test_result run_all_tests();

enum test_result heap_init_test(void** heap_init_addr_pointer);
enum test_result normal_memory_allocation_test(struct block_header* heap_start);
enum test_result free_one_block_test(struct block_header* heap_start);
enum test_result free_two_blocks_test(struct block_header* heap_start);
enum test_result grow_heap_same_region_test(struct block_header* heap_start);
enum test_result grow_heap_another_region_test(struct block_header* heap_start);
#endif //TEST_H
