#define  _DEFAULT_SOURCE

#include "test.h"
#include <unistd.h>



//Определение размера всей кучи
static block_size total_heap_size(struct block_header* heap_start);

//Тест на инициализацию кучи
enum test_result heap_init_test(void** heap_init_addr_pointer) {
    printf("\n"
           "--- Heap initialization test ---\n"
           "TEST STARTED\n");
    size_t query = REGION_MIN_SIZE;
    *heap_init_addr_pointer = heap_init(query);printf("checkpoint 2\n");

    if(*heap_init_addr_pointer == NULL) {
        printf("ERROR: heap wasn't initialized\n");
        return TEST_FAILED;
    }
    printf("Heap initialized successfully\n");
    printf("TEST PASSED\n\n");
    return TEST_PASSED;
}

//Тест на обычное выделение памяти
enum test_result normal_memory_allocation_test(struct block_header* heap_start) {
    printf("--- Normal memory allocation test ---\n"
           "TEST STARTED\n");

    size_t query = 512;

    printf("Allocating one block...\n");
    void* addr = _malloc(query);

    if (addr == NULL){
        printf("ERROR: _malloc returned NULL pointer\n");
        return TEST_FAILED;
    }
    printf("Block allocated successfully.\n");

    //Получаем адрес заголовка блока из выделенного адреса
    struct block_header* allocated_block = block_get_header(addr);

    printf("Heap with allocated block:\n");
    debug_heap(stdout, heap_start);

    if (allocated_block->is_free) {
        printf("ERROR: allocated heap is free, have to be not free\n");
        return TEST_FAILED;
    }

    if (allocated_block->capacity.bytes != query) {
        printf("ERROR: allocated heap has wrong capacity: %zu, expected: %zu\n",
               allocated_block->capacity.bytes,
               query);
        return TEST_FAILED;
    }

    printf("Releasing blocks...\n");
    _free(addr);

    printf("TEST PASSED\n\n");
    return TEST_PASSED;
}

//Тест на освобождение одного блока из нескольких выделенных
enum test_result free_one_block_test(struct block_header* heap_start) {
    printf("--- Free one block test ---\n"
           "TEST STARTED\n");

    size_t query = 256;

    printf("Allocating two blocks...\n");
    void* addr1 = _malloc(query);
    void* addr2 = _malloc(query);

    if (!addr1  || !addr2) {
        printf("ERROR: _malloc() returned NULL pointer\n");
        return TEST_FAILED;
    }
    printf("Blocks allocated successfully\n");

    struct block_header* block1 = block_get_header(addr1);

    printf("Heap before releasing 2nd block:\n");
    debug_heap(stdout, heap_start);
    printf("Releasing 2nd block...\n");
    _free(addr2);
    printf("Heap after releasing 2nd block:\n");
    debug_heap(stdout, heap_start);


    if(!block1->next->is_free) {
        printf("ERROR: released block is not free!\n");
        return TEST_FAILED;
    }

    printf("Releasing blocks...\n");
    _free(addr1);

    printf("TEST_PASSED\n\n");
    return TEST_PASSED;
}

//Освобождение двух блоков из нескольких выделенных
enum test_result free_two_blocks_test(struct block_header* heap_start) {
    printf("--- Free two blocks test ---\n"
           "TEST STARTED\n");

    size_t query = 128;

    printf("Allocating three blocks...\n");
    void* addr1 = _malloc(query);
    void* addr2 = _malloc(query);
    void* addr3 = _malloc(query);

    if (addr1 == NULL || addr2 == NULL || addr3 == NULL) {
        printf("ERROR: _malloc() returned NULL pointer.\n");
        return TEST_FAILED;
    }
    printf("Blocks allocated successfully.\n");

    printf("Heap with allocated blocks:\n");
    debug_heap(stdout, heap_start);

    struct block_header* first_freed_block = block_get_header(addr1);
    struct block_header* second_freed_block = block_get_header(addr3);
    struct block_header* allocated_block = block_get_header(addr2);

    _free(addr1);
    _free(addr3);

    if(!first_freed_block->is_free || !second_freed_block->is_free) {
        printf("ERROR: released blocks still marked as not free.\n");
        return TEST_FAILED;
    }

    if(allocated_block->is_free) {
        printf("ERROR: allocated block marked as free.\n");
        return TEST_FAILED;
    }

    printf("Releasing blocks...\n");
    _free(addr2);

    printf("TEST PASSED\n\n");
    return TEST_PASSED;
}

//Тест на расширение старого региона
enum test_result grow_heap_same_region_test(struct block_header* heap_start) {
    printf("--- Grow heap in same region ---\n"
           "TEST STARTED\n");
    printf("Heap before growing:\n");
    debug_heap(stdout, heap_start);

    size_t query = total_heap_size(heap_start).bytes;

    void* addr = _malloc(query);

    printf("Heap after growing:\n");
    debug_heap(stdout, heap_start);

    if(total_heap_size(heap_start).bytes < 2*query){
        printf("ERROR: allocated not enough memory\n");
        return TEST_FAILED;
    }

    printf("Releasing blocks...\n");
    _free(addr);

    printf("TEST PASSED\n\n");
    return TEST_PASSED;
}

//Тест на выделение нового региона
enum test_result grow_heap_another_region_test(struct block_header* heap_start) {
    printf("--- Grow heap in another region ---\n"
           "TEST STARTED\n");

    struct block_header* last_block = heap_start;

    size_t query = total_heap_size(heap_start).bytes;
    void* addr1 = _malloc(query);

    if (addr1 == NULL) {
        printf("ERROR: _malloc() returned NULL pointer\n");
        return TEST_FAILED;
    }

    printf("Heap before growing:\n");
    debug_heap(stdout, heap_start);

    while (last_block->next != NULL) {
        last_block = last_block->next;
    }

    printf("Trying to map pages right after the last block...\n");
    //Определяем адрес сразу за последним блоком
    void* dummy_addr = (uint8_t*)last_block + size_from_capacity(last_block->capacity).bytes;
    size_t dummy_query = 1024;
    //Определяем размер заглушки согласно размеру страниц памяти
    dummy_query = getpagesize() * (dummy_query / getpagesize() + ((dummy_query % getpagesize()) > 0));
    //Выделяем память по адресу сразу за последним блоком
    dummy_addr = mmap(dummy_addr, dummy_query, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED, 0, 0);

    if (dummy_addr == MAP_FAILED) {
        printf("WARN: pages right after the last block are already mapped\n");
    } else {
        printf("Pages mapped successfully\n");
    }

    printf("Allocating second region...\n");
    void* second_region_addr = _malloc(query);

    printf("Heap after growing:\n");
    debug_heap(stdout, heap_start);

    if(second_region_addr == NULL) {
        printf("ERROR: _malloc() returned NULL pointer\n");
        return TEST_FAILED;
    }

    struct block_header* second_region = block_get_header(second_region_addr);

    if (second_region == dummy_addr) {
        printf("ERROR: second region is located right after the first region\n");
        return TEST_FAILED;
    }


    printf("Releasing blocks...\n");
    //Освобождаем блоки
    _free(addr1);
    _free(second_region_addr);

    //Освобождаем ячейки, выделенные под заглушку
    munmap(dummy_addr, dummy_query);

    printf("TEST PASSED\n\n");
    return TEST_PASSED;
}

enum test_result run_all_tests() {
    void* heap_addr = NULL;
    if(heap_init_test(&heap_addr)) {
        return TEST_FAILED;
    }

    struct block_header* heap_start = (struct block_header*) heap_addr;

    debug_heap(stdout, heap_addr);
    if (normal_memory_allocation_test(heap_start) ||
        free_one_block_test(heap_start) ||
        free_two_blocks_test(heap_start) ||
        grow_heap_same_region_test(heap_start) ||
        grow_heap_another_region_test(heap_start) )
    {
        return TEST_FAILED;
    } else
    {
        printf("All testes passed!\n");
        return TEST_PASSED;
    }
}

static block_size total_heap_size(struct block_header* heap_start) {
    struct block_header* cur_block = heap_start;
    size_t total_size = 0;
    while(cur_block){
        total_size += size_from_capacity(cur_block->capacity).bytes;
        cur_block = cur_block->next;
    }
    return (block_size) {.bytes = total_size};
}
